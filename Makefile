matmul: matriz.c
	gcc -Wall -fsanitize=address,undefined -pthread matriz.c -o matmul

.PHONY: clean
clean:
	rm matmul
run:
	matmul --filas_m1 2000 --cols_m1 800 --filas_m2 800 --cols_m2 3000 --n_hilos 8

gdb:
	gdb matmul
